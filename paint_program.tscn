[gd_scene load_steps=11 format=2]

[ext_resource path="res://draw_line.tscn" type="PackedScene" id=1]

[sub_resource type="GDScript" id=1]
resource_name = "CanvasSize"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Node


export var size : Vector2 = Vector2(512, 512) setget , get_size

func get_size() -> Vector2:
	return size
"

[sub_resource type="GDScript" id=2]
resource_name = "Background"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
tool
extends Node2D


export var size : NodePath = \"../Size\"
onready var _size : Node = get_node(size)


func _draw() -> void:
	
	# Background
	draw_rect(
		Rect2(Vector2(0,0), _size.get_size()),
		Color(1.0, 1.0, 1.0, 0.2),
		true
	)
	
	# Border
	draw_rect(
		Rect2(Vector2(0,0), _size.get_size()),
		Color(0.0, 0.0, 0.0, 0.5),
		false, 2.0, false
	)
"

[sub_resource type="GDScript" id=3]
resource_name = "ViewportContainer"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Control


export var size : NodePath = \"../Size\"
onready var _size : Node = get_node(size)


func _ready() -> void:
	rect_size = _size.get_size()
"

[sub_resource type="GDScript" id=4]
resource_name = "Viewport"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Viewport
# resize the Viewport to the size specified in the \"Size\" data node


export var canvas_size : NodePath = \"../../Size\"
onready var _size : Node = get_node(canvas_size)


func _ready() -> void:
	size = _size.get_size()
"

[sub_resource type="GDScript" id=5]
resource_name = "Pencil"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Node2D

export var draw_line : PackedScene
export var viewport : NodePath = \"../..\"
onready var _viewport : Viewport = get_node(viewport) setget , get_viewport
export var line_color : Color = Color(0.0, 0.0, 0.0, 1.0) \\
		setget , get_color
export var line_thickness : float = 10.0 setget , get_thickness


func get_color() -> Color:
	return line_color

func get_thickness() -> float:
	return line_thickness

func get_viewport() -> Viewport:
	return _viewport


func _input(event) -> void:
	if event is InputEventScreenTouch and event.is_pressed():
		var _line = draw_line.instance()
		add_child(_line)
"

[sub_resource type="GDScript" id=6]
resource_name = "RemoveOnNonMobile"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Node

export var parent : NodePath = \"..\"
onready var _parent : Node = get_node(parent)


func _ready() -> void:
	if not (OS.get_name() == \"Android\" or OS.get_name() == \"iOS\"):
		_parent.queue_free()
	else:
		self.queue_free()
"

[sub_resource type="GDScript" id=7]
resource_name = "MobileBuffer"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Sprite

export var viewport : NodePath = \"../../..\"
onready var _viewport : Viewport = get_node(viewport)


func _ready() -> void:
	if OS.get_name() == \"Android\" or OS.get_name() == \"iOS\":
		# enclosing the code in this if-statement does nothing
		# except for removing the following error from the debugger:
			# E 0:00:00.315   resume: Resumed function '_ready()' after yield,
					# but script is gone. At script:
					# res://paint_program.tscn::10:11
			#   <C++-Fehler>  Method failed. Returning: Variant()
			#   <C++-Quellcode>modules/gdscript/gdscript_function.cpp:1791
					# @ resume()
		yield(get_tree(), \"idle_frame\")
				# this lines fixes a rendering bug
				# where on every frame
				# the drawing gets duplicated to another position
		set_position(_viewport.get_size() / 2)


func _input(event) -> void:
	if event is InputEventScreenTouch and not event.is_pressed():
		texture = _viewport.get_texture()
"

[sub_resource type="GDScript" id=8]
resource_name = "ClearLines"
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Node

export var pencil : NodePath = \"../../Pencil\"
onready var _pencil : Node2D = get_node(pencil)


func _input(event) -> void:
	if event is InputEventScreenTouch and not event.is_pressed():
		for i in _pencil.get_children():
			_pencil.remove_child(i)
			i.queue_free()
"

[sub_resource type="GDScript" id=9]
script/source = "# This code is MIT-licensed: \\
#		https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE
# Source: https://codeberg.org/Sosasees/simplest-paint-in-godot/
extends Node


export var viewport : NodePath = \"..\"
onready var _viewport : Viewport = get_node(viewport)


# Uncomment this function to automatically save your drawing after 4 seconds
# in the root of the project, for demonstration purposes.
# Don't forget to comment out this function again.
#func _ready() -> void:
#	yield(get_tree().create_timer(4.0), \"timeout\")
#	print_debug(\"The Drawing is saved to the Project Root.\")
#	printerr(\"Don't forget to comment out this function again.\")
#	save(\"res://demo_image.png\")


func save(path : String):
# warning-ignore:return_value_discarded
	_viewport.get_texture().get_data().save_png(path)
"

[node name="PaintProgram" type="Node2D"]

[node name="Canvas" type="Node2D" parent="."]
position = Vector2( 32, 32 )

[node name="Size" type="Node" parent="Canvas"]
script = SubResource( 1 )

[node name="Background" type="Node2D" parent="Canvas"]
script = SubResource( 2 )
__meta__ = {
"_editor_description_": "draws the Background Rectangle for the canvas"
}

[node name="ViewportContainer" type="ViewportContainer" parent="Canvas"]
margin_right = 512.0
margin_bottom = 512.0
script = SubResource( 3 )
__meta__ = {
"_edit_use_anchors_": false,
"_editor_description_": "resize the ViewportContainer to the size specified in the \"Size\" data node"
}

[node name="Viewport" type="Viewport" parent="Canvas/ViewportContainer"]
size = Vector2( 500, 500 )
transparent_bg = true
handle_input_locally = false
disable_3d = true
usage = 1
render_target_v_flip = true
render_target_update_mode = 3
script = SubResource( 4 )

[node name="Draw" type="Node2D" parent="Canvas/ViewportContainer/Viewport"]

[node name="Pencil" type="Node2D" parent="Canvas/ViewportContainer/Viewport/Draw"]
script = SubResource( 5 )
__meta__ = {
"_editor_description_": "Instances the Lines that get drawn when touching the Screen"
}
draw_line = ExtResource( 1 )

[node name="MobileOptimization" type="Node2D" parent="Canvas/ViewportContainer/Viewport/Draw"]
__meta__ = {
"_editor_description_": "Optimizes the program on mobile platforms"
}

[node name="RemoveOnNonMobile" type="Node" parent="Canvas/ViewportContainer/Viewport/Draw/MobileOptimization"]
script = SubResource( 6 )
__meta__ = {
"_editor_description_": "removes the parent node on non-mobile platforms
(otherwise removes itself because it's useless)"
}

[node name="Buffer" type="Sprite" parent="Canvas/ViewportContainer/Viewport/Draw/MobileOptimization"]
script = SubResource( 7 )
__meta__ = {
"_editor_description_": "each line gets saved to this \"buffer\" texture
so that they don't have to be redrawn"
}

[node name="ClearLines" type="Node" parent="Canvas/ViewportContainer/Viewport/Draw/MobileOptimization"]
script = SubResource( 8 )
__meta__ = {
"_editor_description_": "clears the lines after they've been saved to the buffer"
}

[node name="Save" type="Node" parent="."]
script = SubResource( 9 )
viewport = NodePath("../Canvas/ViewportContainer/Viewport")
