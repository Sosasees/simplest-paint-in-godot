⚠ the Godot 3 edition of this project is obseleted.
please see the [Godot 4 edition](https://codeberg.org/sosasees/simplest-paint-in-godot-4)

# Simplest "Paint" in Godot 3

This version of the project is made in and for
[Godot](https://godotengine.org) `v3.4.stable.official [206ba70f4]`.

This Godot Project is my [MIT-licensed](
https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE)
"Paint" drawing program
that is made to be as simple as possible
so that it can be used in many other Godot projects.

[▶️ Watch Video Demonstration](.readme-assets/demo.webm)

## What is this program good for?

the simplicity makes this program barely suitable as a
standalone drawing program.

but it's also this program's greatest strength
because only having the most necessary features lets it be integrated
into all kinds of other Godot projects.

## Features

### Most Important

- the Pencil
- - click and drag to draw black lines
- the Drawing Canvas
- - is in its own viewport
    so it can be positioned much more freely in the scene
- use of Godot's node-based design taken to its advantage for clean code
- - this makes it much easier to make your own programs based on this one

### Hidden Save Feature

- Save feature
- - inaccessible by Default
- - open the project and comment out the Demonstration Snippet
    in the "Save" node's script
    to try this hidden feature
- - - saves the current drawing to the Project Root `res://`
      4 seconds after starting the program
- - - - after trying it, please delete the just-saved drawing
        and change the behavior for your own use-case

### Platform Differences in Optimization

- on Mobile, every time you finish drawing a line,
  the picture gets saved to a "buffer" texture before removing all "Line" nodes
  to get back near-full performance
- - on Desktop, this behavior causes rendering glitches,
    and the extra performance is not needed, so
    every new line simply gets drawn on top of the other "Line" nodes instead.
    no need to mess with a "buffer" texture or removing the "Line" nodes.
